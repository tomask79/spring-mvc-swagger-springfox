# Documenting REST Web Services using Spring Fox #

I bet everyone of you have heard about **Soap Web Services** before. So you know that to get the details about particular SOAP web service, you need to get **WSDL document** describing what that SOAP Web Service consumes and what produces. But what about REST Web services? **Is there a standard protocol of how to describe interface of Rest Web Service? Simply said, it isn't.** 

When consuming REST service, you will probably know just that there is somewhere some endpoint exposing some JSON and that's all...Let's take a look 
on currently most used tools for describing REST services **where using each of them depends on the nature of your project!**

* *Swagger* (Probably most popular)
* *RAML* (Rest Api Markup Language, Yaml style REST documentation)
* *ApiDocs* (Writing the doc directly upon the controllers methods)

As usual, I'm concerned about Spring support...

* *SpringFox* (Scans for SpringMVC annotations from running SpringMVC App, no additional code required! Keeps the code cleaner IMHO. Supports Swagger 2.0)
* *Swagger* maven plugin (Doesn't know anything about SpringMVC, you write Swagger annotations into code...Can end in annotations hell eventually)
* *SpringRestDOCs* (Doc is generated according the test code in ASCII doctor format)

## Swagger with SpringFox ##

SpringFox is able to detect SpringMVC annotations from existing code and that application needs to be running. In addition, to tweak the final description of REST Service, SpringFox gives you to use Swagger-Core annotations. For example like in this demo:


```
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.mvc.pojo.Person;
import com.example.mvc.pojo.Persons;

@RestController
public class PersonsController {
  
	@RequestMapping("/persons")
	@ApiOperation(value="", nickname="getMeAllPersons")
	public Persons getPersons() {
		final List<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Jack", "Cahill", "QA"));
		persons.add(new Person("John", "BorrowMan", "Development"));
		
		return new Persons(persons);
	}
}
```
ApiOperation is the swagger-core annotation for giving the REST service method a nickname. To configure SpringFox-Swagger in the application you need to construct [Docket](http://springfox.github.io/springfox/javadoc/current/springfox/documentation/spring/web/plugins/Docket.html) object, where you will tell SpringFox-Swagger what REST endpoints will be documented...For example:


```
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan
public class SwaggerSpringMvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerSpringMvcApplication.class, args);
    }
    
    @Bean
    public Docket personsAPI() {
      return new Docket(DocumentationType.SWAGGER_2)
          .select()
            // Any kind of controller's methods, inside of any package...
            .apis(RequestHandlerSelectors.any()) 
            // Scan for endpoints containing this URL pattern
            .paths(PathSelectors.regex("/persons/*")) 
            .build()
           // Prefix for all founded enpoints, used patterns prefixes all founded endpoints.
          .pathMapping("/");
    }
}
```


## Testing this demo ##

This **demo contains simple Spring Boot application with REST controller(returning persons) and SpringFox exposing documentation** of that controller via web interface. How to run it:

* git clone <this repository>
* mvn clean install
* java -jar target/demo-0.0.1-SNAPSHOT.jar

Now hit the following URL:

```
http://localhost:8080/swagger-ui.html
```
You will see Swagger-UI page containing description of our REST service. You can even test the REST service.

Now hit the following URL:

```
http://localhost:8080/v2/api-docs
```
You will obtain JSON presentation of our REST service. No big deal you'll say probably, but with project [swagger-codegen](https://github.com/swagger-api/swagger-codegen) you can generate REST Client from this JSON presentation.

Simply run:

```
java -jar modules/swagger-codegen-cli/target/swagger-codegen-cli.jar generate \
  -i http://localhost:8080/v2/api-docs \
  -l spring-mvc \
  -o samples/mvc-persons
```
IMHO SpringFox suits perfectly for large developing of REST services between front-end and back-end teams. Nice work.