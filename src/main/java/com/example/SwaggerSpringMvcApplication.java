package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan
public class SwaggerSpringMvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerSpringMvcApplication.class, args);
    }
    
    @Bean
    public Docket personsAPI() {
      return new Docket(DocumentationType.SWAGGER_2)
          .select()
          	// Any kind of methods, inside of any package...
            .apis(RequestHandlerSelectors.any()) 
            // Scan for endpoints containing this URL pattern
            .paths(PathSelectors.regex("/persons/*")) 
            .build()
           // Prefix for all founded enpoints, used patterns prefixes all founded endpoints.
          .pathMapping("/");
    }
}
