package com.example.mvc;

import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.mvc.pojo.Person;
import com.example.mvc.pojo.Persons;

@RestController
public class PersonsController {
  
	@RequestMapping("/persons")
	@ApiOperation(value="", nickname="getMeAllPersons")
	public Persons getPersons() {
		final List<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Jack", "Cahill", "QA"));
		persons.add(new Person("John", "BorrowMan", "Development"));
		
		return new Persons(persons);
	}
}
